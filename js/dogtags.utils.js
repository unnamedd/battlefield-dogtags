var DT = DT || {};

(function($){
    DT.Utils = DT.Utils || {};
    DT.Utils.Ajax = (function(){
        var fetch = function(type, url, params, dataType, cache, async, contenttype){
            var settings = {};
            var options = {};

            var defaults = {
                type: type,
                url: url,
                data: params,
                dataType: dataType,
                cache: cache,
                async: async
            };
            if (contenttype !== null)
                options["contentType"] = contenttype;

            settings = $.extend(defaults, options);

            return $.ajax(settings);
        };
        var getAsync = function(url, params){
            return fetch("GET", url, params, "json", false, true);
        };

        var getSync = function(url, params){
            return fetch("GET", url, params, "json", false, false);
        };

        var postAsync = function(url, params){
            return fetch("POST", url, params, "json", false, true);
        };

        var postSync = function(url, params){
            return fetch("POST", url, params, "json", false, false);
        };

        var postJsonAsync = function(url, params) {
            return fetch("POST", url, params, "json", false, true, "application/json");
        };

        var postJsonSync = function(url, params) {
            return fetch("POST", url, params, "json", false, false, "application/json");
        };

        return {
            getAsync    :   getAsync,
            getSync     :   getSync,
            postAsync   :   postAsync,
            postSync    :   postSync,
            postJsonSync:   postJsonSync,
            postJsonAsync:   postJsonAsync
        };
    })();

    DT.Utils.Deferred = function(){
        var deferred = $.Deferred();

        deferred.badRequest = function(message){
            deferred.reject(400, (message || "Bad Request") );
        };

        deferred.unauthorized = function(message){
            deferred.reject(401, (message || "Not Authorized") );
        };

        deferred.forbidden = function(message){
            deferred.reject(403, (message || "Forbidden") );
        };

        deferred.notFound = function(message){
            deferred.reject(404, (message || "Not Found"));
        };

        deferred.internalError = function(message){
            deferred.reject(500, (message || "Internal Server Error"));
        };

        return (deferred);
    };
})(jQuery);

if (!window.location.parameters)
    window.location.parameters = (function () {
        var qs = document.location.search.split("+").join(" ");
        var params = {}, tokens,
            re = /[?&]?([^=]+)=([^&]*)/g;

        while (tokens = re.exec(qs)) {
            if (/\[/.test(decodeURIComponent(tokens[1])) && /\]/.test(decodeURIComponent(tokens[1]))){
                var groups = /([A-Za-z]+)\[([^\]]+)\]/.exec(decodeURIComponent(tokens[1]));
                if (typeof params[groups[1]] == "undefined")
                    params[groups[1]] = {};
                params[groups[1]][groups[2]] = tokens[2];
            }
            else {
                params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);
            }
        }
        return params;
    })();

if (!String.prototype.format)
    String.prototype.format = function () {
        var pattern = /\{\d+\}/g;
        var args = arguments;
        return this.replace(pattern, function (capture) { return args[capture.match(/\d+/)]; });
    };
