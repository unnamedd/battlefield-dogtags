var requestList = function(player){
	DT.Data.DogTags.loadFromPlayer(player).done(function(data){
		console.log("Status: {0}".format(data.status));
	}).fail(function(statusCode, message){
		console.log("Status: {0} - Message: {1}".format(statusCode, message));
	});
};

$(document).ready(function(){
	if (!_.isNull(window.location.parameters) && !_.isEmpty(window.location.parameters.player)){
		var player = window.location.parameters.player;
		requestList(player);
	}
});