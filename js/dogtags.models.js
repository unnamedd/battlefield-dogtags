var urlListaDogTagsToPlayer = "http://api.bf3stats.com/pc/dogtags/";
var urlMockListaDogTagsToPlayerSuccess = "http://localhost:8000/mock/dogtags.json";
var urlMockListaDogTagsToPlayerNotFound = "http://localhost:8000/mock/notfound.json";
var urlMockListaDogTagsToPlayerError = "http://localhost:8000/mock/error.json";


var User = Backbone.Model.extend({
	defaults: function(){
		return {
			playername: "empty nick",
			platform: "no platform"
		};
	}
});

var DT = DT || {};
(function($){
	DT.Data = DT.Data || {};
	DT.Data.DogTags = (function(){
		var loadFromPlayer = function(player) {
			var response = DT.Utils.Deferred();
			var url;

			if (_.isEmpty(player))
				url = urlMockListaDogTagsToPlayerError; // when the 'player' parameter is empty
			else if (player == "cobra-mrgoogle")
				url = urlMockListaDogTagsToPlayerSuccess; // when the 'player' is equal to 'COBRA-MrGoogle'
			else
				url = urlMockListaDogTagsToPlayerNotFound; // when the 'player' is not equal to 'COBRA-MrGoogle'

			DT.Utils.Ajax.getAsync(url, {player: player}).success(function(data){
				if (data.status == "dogtags")
					response.resolve(data);

				else {
					if (data.status == "notfound")
						response.notFound("player was not found");

					else if (data.status == "error")
						response.badRequest(data);
				}
			});

			return response.promise();
		};

		return {
			loadFromPlayer: loadFromPlayer
		};
	})();
})(jQuery);